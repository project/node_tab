<?php

/**
 * @file
 * Plugin to provide an relationship handler for tab from node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Node tab from node'),
  'keyword' => 'node_tab',
  'description' => t('Adds a Node tab from a node context.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'node_tab_from_node_context',
  'edit form' => 'node_tab_from_node_settings_form',
  // Even if we're using the machine name, this has to be called tab_id,
  // otherwise, it won't work.
  'defaults' => array('tab_id' => ''),
);

/**
 * Return a new context based on an existing context.
 */
function node_tab_from_node_context($context, $conf) {
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:node_tab', NULL);
  }
  $tab = node_tab_load($context->data->type, $conf['tab_id']);

  if (!$tab) {
    return FALSE;
  }

  return ctools_context_create('entity:node_tab', $tab);
}

/**
 * Settings form for the relationship.
 */
function node_tab_from_node_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $options = array();

  foreach (entity_load('node_tab') as $tab) {
    $options[$tab->name] = t('@label (@machine_name)', array('@label' => entity_label('node_tab', $tab), '@machine_name' => $tab->name));
  }

  $form['tab_id'] = array(
    '#title' => t('Tab name'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['tab_id'],
    '#prefix' => '<div class="clearfix">',
    '#suffix' => '</div>',
  );

  return $form;
}
